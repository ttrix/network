# SSH

## useful command

### Generate ssh key

ssh-keygen

### add ssh key to a server to authorize access/connection with (digital ocean only?)

add key in:

```shell
vim /root/.ssh/authorized_keys
```

This file lets the server authenticate the user

after the first connection the ssh key is stored in server in:
‘‘‘shell
/root/.ssh/known_hosts
‘‘‘
Let's the client authenticate the server to check that it isn't connecting to an impersonator

### connect to a server using ssh

ssh user@server_ip_address
ie. ssh root@10.10.10.10

if we want to provide a different location for our id_rsa file like this
ssh -i ~/.ssh/id_rsa root@10.10.10.10

### copy files to a server using ssh

scp file_to_copy user@server_ip_address:/location
scp file_to_copy.sh root@10.10.10.10:/root
