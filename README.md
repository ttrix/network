# Networks

IP Address
Switch
Routher
Gateway address (or routher IP address)
DNS
Firewall

FQDN: Fully Qualified Domain Name

## Useful commands

-> ifconfig: see network configuration
- ip address
- subnet mask
- gateway address (routher IP address)

-> netstat: show active connection in the machine
(which applications are actively listenning on the specific ports)
typical use case: when an application in the server is not accessible and we are getting connection refused for example, we can check with this command wheter the application is running or not and at which port is listenning

-> ps aux: see the processes running in the machine

-> nslookup: get the ip address of any domain name and the Ip address of the DNS server being used by the machine
We can also do a reverse check to show what domain name is attached to a specific IP address

-> ping: Checks whether a service or application is accessible by using the ip address or domain name
